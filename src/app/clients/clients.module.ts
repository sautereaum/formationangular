import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ClientsRoutingModule } from './clients-routing.module';
import { ClientComponent } from './components/client/client.component';
import { AddClientComponent } from './containers/add-client/add-client.component';
import { FormClientsComponent } from './containers/form-clients/form-clients.component';
import { ListClientComponent } from './containers/list-client/list-client.component';
import { PageAddClientComponent } from './pages/page-add-client/page-add-client.component';
import { PageClientsComponent } from './pages/page-clients/page-clients.component';

@NgModule({
    declarations: [
        PageClientsComponent,
        ListClientComponent,
        ClientComponent,
        PageAddClientComponent,
        FormClientsComponent,
        AddClientComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ClientsRoutingModule,
        FormsModule
    ]
})
export class ClientsModule {}
