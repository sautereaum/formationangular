import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StateClient } from '../../../shared/enums/state-client.enum';
import { ClientI } from '../../../shared/interfaces/client.interface';

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

    @Input()
    client: ClientI;
    states = StateClient;

    @Output()
    stateChange = new EventEmitter<{ client: ClientI, state: StateClient }>();

    constructor() { }

    ngOnInit() {
    }

    changeState($event: any, client: ClientI) {
        this.stateChange.emit({ client: client, state: $event.target.value });
    }

}
