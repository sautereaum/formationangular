import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientI } from '../../../shared/interfaces/client.interface';
import { ClientService } from '../../services/client.service';

@Component({
    selector: 'app-add-client',
    templateUrl: './add-client.component.html',
    styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
    btnTitle: string;

    constructor(private clientService: ClientService,
                private router: Router,
                private route: ActivatedRoute) { }

    ngOnInit() {
        this.btnTitle = "Ajouter le client";
    }

    onSubmit($event: ClientI) {
        this.clientService.add($event);
        this.router.navigate(['../'], {relativeTo: this.route});
    }
}
