import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StateClient } from '../../../shared/enums/state-client.enum';
import { ClientI } from '../../../shared/interfaces/client.interface';
import { Client } from '../../../shared/models/client.model';

@Component({
    selector: 'app-form-clients',
    templateUrl: './form-clients.component.html',
    styleUrls: ['./form-clients.component.scss']
})
export class FormClientsComponent implements OnInit {
    @Input()
    btnTitle: string;
    @Output()
    submit = new EventEmitter<ClientI>();

    states = StateClient;

    init = new Client();

    constructor() { }

    ngOnInit() {
    }

    onFormSubmit() {
        this.submit.emit(this.init);
    }
}
