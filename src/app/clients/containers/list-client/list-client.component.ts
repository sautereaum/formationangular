import { Component, OnInit } from '@angular/core';
import { StateClient } from '../../../shared/enums/state-client.enum';
import { ClientI } from '../../../shared/interfaces/client.interface';
import { ClientService } from '../../services/client.service';

@Component({
    selector: 'app-list-client',
    templateUrl: './list-client.component.html',
    styleUrls: ['./list-client.component.scss']
})
export class ListClientComponent implements OnInit {

    clients: ClientI[];
    headers: string[];

    constructor(private clientService: ClientService) { }

    ngOnInit() {
        this.clients = this.clientService.clients;
        this.headers = ['Nom', 'Email', 'Statut'];
    }

    onStateChange($event: { client: ClientI; state: StateClient }) {
        this.clientService.update($event.client, { state: $event.state });
    }
}
