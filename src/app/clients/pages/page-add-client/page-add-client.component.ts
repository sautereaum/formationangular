import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-page-add-client',
    templateUrl: './page-add-client.component.html',
    styleUrls: ['./page-add-client.component.scss']
})
export class PageAddClientComponent implements OnInit {
    title: string;
    subTitle: string;
    btnTitle: string;

    constructor() { }

    ngOnInit() {
        this.btnTitle = "Ajouter le client";
        this.title = "Page d'ajout de client";
        this.subTitle = "Parce que les clients c'est bien !";
    }

    onFormSubmit($event: {}) {
        console.log("formulaire add client soumis")
    }
}
