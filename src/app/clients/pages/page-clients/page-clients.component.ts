import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-page-clients',
    templateUrl: './page-clients.component.html',
    styleUrls: ['./page-clients.component.scss']
})
export class PageClientsComponent implements OnInit {
    title: string;
    subTitle: string;
    addBtnText: string;

    constructor() { }

    ngOnInit() {
        this.title = "Clients";
        this.subTitle = "Nos clients";
        this.addBtnText = "Ajouter un client";
    }

}
