import { Injectable } from '@angular/core';
import { ClientI } from '../../shared/interfaces/client.interface';
import { fakeClient } from './fake-client';

@Injectable({
    providedIn: 'root'
})
export class ClientService {

    constructor() {
        this.clients = fakeClient;
    }

    private _clients: ClientI[];

    get clients(): ClientI[] {
        return this._clients;
    }

    set clients(clients: ClientI[]) {
        this._clients = clients;
    }

    update(client: ClientI, fields: Partial<ClientI>) {
        Object.assign(client, fields);
    }

    add(client: ClientI) {
        this.clients.push(client)
    }
}
