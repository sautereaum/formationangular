import { ClientI } from '../../shared/interfaces/client.interface';
import { Client } from '../../shared/models/client.model';

export const fakeClient: ClientI[] = [
    new Client({
        id: 'aeigbaef',
        name: 'Capgemini',
        email: 'services@capgemini.fr'
    }),
    new Client({
        id: 'aegfafe',
        name: 'Atos',
        email: 'services@atos.fr'
    }),
    new Client({
        id: 'zryswdss',
        name: 'Edf',
        email: 'services@edf.fr'
    })
];