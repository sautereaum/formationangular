import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageLoginComponent } from './pages/page-login/page-login.component';


const loginRoutes: Routes = [
    {
        path: 'login',
        component: PageLoginComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(
            loginRoutes
        )
    ]
})
export class LoginRoutingModule {}
