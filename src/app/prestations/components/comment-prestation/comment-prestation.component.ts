import { Component, OnInit } from '@angular/core';
import { PrestationService } from '../../services/prestation.service';

@Component({
    selector: 'app-comment-prestation',
    templateUrl: './comment-prestation.component.html',
    styleUrls: ['./comment-prestation.component.scss']
})
export class CommentPrestationComponent implements OnInit {

    constructor(private prestationService: PrestationService) { }

    ngOnInit() {

    }

}
