import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { PrestationI } from '../../../shared/interfaces/prestation.interface';
import { PrestationService } from '../../services/prestation.service';

@Component({
    selector: 'app-detail-prestation',
    templateUrl: './detail-prestation.component.html',
    styleUrls: ['./detail-prestation.component.scss']
})
export class DetailPrestationComponent implements OnInit {

    prestation: BehaviorSubject<PrestationI>;

    constructor(private router: ActivatedRoute,
                private prestationService: PrestationService) {
    }

    ngOnInit() {
        this.prestation = this.prestationService.detail;
    }

}
