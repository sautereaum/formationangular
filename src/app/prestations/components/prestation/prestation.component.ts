import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { States } from '../../../shared/enums/states.enum';
import { PrestationI } from '../../../shared/interfaces/prestation.interface';

@Component({
    selector: 'app-prestation',
    templateUrl: './prestation.component.html',
    styleUrls: ['./prestation.component.scss']
})
export class PrestationComponent implements OnInit {

    @Input()
    prestation: PrestationI;
    @ViewChild('type')
    type: ElementRef<any>;

    @Output()
    stateChange = new EventEmitter<{ prestation: PrestationI, state: States }>();

    @Output()
    clicked = new EventEmitter<{ item: PrestationI, ref: ElementRef<any> }>();

    states = States;
    //
    // faEdit: faEdit;

    constructor(private router: Router) { }

    ngOnInit() {
    }

    changeState($event: any) {
        this.stateChange.emit({ prestation: this.prestation, state: $event.target.value });
    }

    onSelect(prestation: PrestationI) {
        this.clicked.emit({ item: prestation, ref: this.type });
    }

    goToDetail() {
        this.router.navigate(['prestations/edit', this.prestation.id])
    }
}
