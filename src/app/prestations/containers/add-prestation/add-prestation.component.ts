import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PrestationI } from '../../../shared/interfaces/prestation.interface';
import { PrestationService } from '../../services/prestation.service';

@Component({
    selector: 'app-add-prestation',
    templateUrl: './add-prestation.component.html',
    styleUrls: ['./add-prestation.component.scss']
})
export class AddPrestationComponent implements OnInit {

    btnTitle: string;

    constructor(private prestationService: PrestationService,
                private router: Router,
                private route: ActivatedRoute) { }

    ngOnInit() {
        this.btnTitle = 'Ajouter la prestation';
    }

    onSubmit($event: PrestationI) {
        this.prestationService.add($event).then(() => {
            this.router.navigate(['../'], {relativeTo: this.route});
        });
    }
}
