import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PrestationI } from '../../../shared/interfaces/prestation.interface';
import { PrestationService } from '../../services/prestation.service';

@Component({
    selector: 'app-edit-prestation',
    templateUrl: './edit-prestation.component.html',
    styleUrls: ['./edit-prestation.component.scss']
})
export class EditPrestationComponent implements OnInit {
    btnTitle: string;
    prestation$: Observable<PrestationI>;

    constructor(private prestationService: PrestationService,
                private route: ActivatedRoute,
                private router: Router) { }

    ngOnInit() {
        this.btnTitle = 'Valider';
        console.log("onInit");
        this.route.paramMap.subscribe(e => {
            this.prestation$ = this.prestationService.getPrestation(e.get('id'));
            console.log(this.prestation$);
        });
    }

    onSubmit($event: PrestationI) {
        this.prestationService.update($event).then(() => {
            this.router.navigate(['prestations']);
        })
    }
}
