import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { States } from '../../../shared/enums/states.enum';
import { PrestationI } from '../../../shared/interfaces/prestation.interface';
import { Prestation } from '../../../shared/models/prestation.model';

@Component({
    selector: 'app-form-presta',
    templateUrl: './form-presta.component.html',
    styleUrls: ['./form-presta.component.scss']
})
export class FormPrestaComponent implements OnInit {

    @Input()
    btnTitle: string;
    @Output()
    submit = new EventEmitter<PrestationI>();

    states = States;
    init = new Prestation();
    form: FormGroup;

    constructor(private builder: FormBuilder) { }

    ngOnInit() {
        this.form = this.builder.group({
            typePresta: [this.init.typePresta, Validators.compose([Validators.required, Validators.minLength(3)])],
            client: [this.init.client],
            tjmHt: [this.init.tjmHt],
            nbJours: [this.init.nbJours],
            tauxTva: [this.init.tauxTva],
            state: [this.init.state],
            comment: [this.init.comment],
        });

        // this.form = this.builder.group(Object.keys(this.init).reduce((p, c) => ({
        //     ...p,
        //     [c]: [`${ this.init[c] || '' }`, this.getValidator(c)]
        // }), {}));
    }

    onFormSubmit() {
        this.submit.emit(new Prestation(this.form.value));
    }

}
