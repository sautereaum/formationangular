import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { States } from '../../../shared/enums/states.enum';
import { PrestationI } from '../../../shared/interfaces/prestation.interface';
import { Prestation } from '../../../shared/models/prestation.model';

@Component({
    selector: 'app-form-prestation',
    templateUrl: './form-prestation.component.html',
    styleUrls: ['./form-prestation.component.scss']
})
export class FormPrestationComponent implements OnInit {

    @Input()
    btnTitle: string;
    @Output()
    submitForm = new EventEmitter<PrestationI>();

    states = States;

    @Input()
    init = new Prestation();

    constructor() { }

    ngOnInit() { }

    onFormSubmit() {
        this.submitForm.emit(this.init);
    }

}