import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { Observable } from 'rxjs';
import { States } from '../../../shared/enums/states.enum';
import { PrestationI } from '../../../shared/interfaces/prestation.interface';
import { PrestationService } from '../../services/prestation.service';

@Component({
    selector: 'app-list-prestation',
    templateUrl: './list-prestation.component.html',
    styleUrls: ['./list-prestation.component.scss']
})
export class ListPrestationComponent implements OnInit {

    prestations$: Observable<PrestationI[]>;
    headers: string[];
    private lastRef: ElementRef<any>;

    constructor(private prestationService: PrestationService,
                private renderer: Renderer2) { }

    ngOnInit() {
        this.prestations$ = this.prestationService.prestations$;
        this.headers = ['Type', 'Client', 'NbJours', 'Tjm HT', 'Total HT', 'Total TTC', 'Statut', '']
    }

    onStateChange($event: { prestation: PrestationI; state: States }) {
        this.prestationService.update($event.prestation, { state: $event.state }).catch(e => console.log(e));
    }

    onClick($event: {item: PrestationI; ref: ElementRef<any>}) {
        if (this.lastRef) {
            this.renderer.removeClass(this.lastRef.nativeElement, 'active');
        }
        this.renderer.addClass($event.ref.nativeElement, 'active');
        this.prestationService.detail.next($event.item);
        this.lastRef = $event.ref;
    }
}
