import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-page-add-prestation',
    templateUrl: './page-add-prestation.component.html',
    styleUrls: ['./page-add-prestation.component.scss']
})
export class PageAddPrestationComponent implements OnInit {
    btnTitle: string;
    title: string;

    constructor() { }

    ngOnInit() {
        this.btnTitle = "Ajouter la prestation";
        this.title = "Page d'ajout de prestation";
    }
}
