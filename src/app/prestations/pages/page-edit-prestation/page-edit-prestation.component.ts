import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-page-edit-prestation',
    templateUrl: './page-edit-prestation.component.html',
    styleUrls: ['./page-edit-prestation.component.scss']
})
export class PageEditPrestationComponent implements OnInit {
    title: string;

    constructor() { }

    ngOnInit() {
        this.title = "Editer la prestation"
    }

}
