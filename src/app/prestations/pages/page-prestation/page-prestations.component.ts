import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-page-prestations',
    templateUrl: './page-prestations.component.html',
    styleUrls: ['./page-prestations.component.scss']
})
export class PagePrestationsComponent implements OnInit {

    title: string;
    addBtnText: string;
    routes: any[];

    constructor() { }

    ngOnInit() {
        this.title = 'Prestations';
        this.addBtnText = 'Ajouter une prestation';
        this.routes = [{ url: 'detail', name: 'Détail' }, { url: 'comment', name: 'Commentaire' }]
    }

}
