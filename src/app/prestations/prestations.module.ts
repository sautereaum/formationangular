import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CommentPrestationComponent } from './components/comment-prestation/comment-prestation.component';
import { DetailPrestationComponent } from './components/detail-prestation/detail-prestation.component';
import { PrestationComponent } from './components/prestation/prestation.component';
import { AddPrestationComponent } from './containers/add-prestation/add-prestation.component';
import { EditPrestationComponent } from './containers/edit-prestation/edit-prestation.component';
import { FormPrestaComponent } from './containers/form-presta/form-presta.component';
import { FormPrestationComponent } from './containers/form-prestation/form-prestation.component';
import { ListPrestationComponent } from './containers/list-prestation/list-prestation.component';
import { PageAddPrestationComponent } from './pages/page-add-prestation/page-add-prestation.component';
import { PageEditPrestationComponent } from './pages/page-edit-prestation/page-edit-prestation.component';
import { PagePrestationsComponent } from './pages/page-prestation/page-prestations.component';
import { PrestationsRoutingModule } from './prestations-routing.module';

@NgModule({
    declarations: [
        PagePrestationsComponent,
        ListPrestationComponent,
        PrestationComponent,
        PageAddPrestationComponent,
        FormPrestationComponent,
        AddPrestationComponent,
        FormPrestaComponent,
        DetailPrestationComponent,
        CommentPrestationComponent,
        PageEditPrestationComponent,
        EditPrestationComponent
    ],
    imports: [
        CommonModule,
        PrestationsRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class PrestationsModule {}
