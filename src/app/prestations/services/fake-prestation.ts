import { Prestation } from '../../shared/models/prestation.model';

export const fakePrestation: Prestation[] = [
    new Prestation({
        id: 'aeiohe',
        typePresta: 'Formation',
        client: 'Capgemini',
        tjmHt: 580,
        nbJours: 56,
        comment: 'Commentaire pour Cap'
    }),
    new Prestation({
        id: 'aeiohe',
        typePresta: 'Lead dev',
        client: 'Atos',
        tjmHt: 1500,
        nbJours: 40,
        comment: 'Commentaire pour Atos'
    }),
    new Prestation({
        id: 'aegasdv',
        typePresta: 'Tech management',
        client: 'Edf',
        tjmHt: 4000,
        nbJours: 32,
        comment: 'Commentaire pour Edf'
    })
];