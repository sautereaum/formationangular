import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrestationI } from '../../shared/interfaces/prestation.interface';
import { Prestation } from '../../shared/models/prestation.model';

@Injectable({
    providedIn: 'root'
})
export class PrestationService {

    private prestationsCollection: AngularFirestoreCollection<PrestationI>;
    private _prestations$: Observable<PrestationI[]>;
    detail: BehaviorSubject<PrestationI>;

    constructor(private fireStore: AngularFirestore) {
        this.prestationsCollection = fireStore.collection<PrestationI>('prestations');
        this.detail = new BehaviorSubject(null);
        this.prestations$ = this.prestationsCollection.valueChanges().pipe(
            map((data => {
                this.detail.next(data[0]);
                return data.map(prestation => new Prestation(prestation));
            }))
        );
    }

    get prestations$(): Observable<PrestationI[]> {
        return this._prestations$;
    }

    set prestations$(prestations$: Observable<PrestationI[]>) {
        this._prestations$ = prestations$;
    }

    update(prestation: PrestationI, fields?: Partial<PrestationI>): Promise<any> {
        const presta = { ...prestation };
        if (fields) {
            Object.assign(presta, fields);
        }
        return this.prestationsCollection.doc(prestation.id).update(presta);
    }

    add(item: PrestationI): Promise<any> {
        const id = this.fireStore.createId();
        const prestation = { ...item };
        prestation.id = id;
        console.log(prestation);
        return this.prestationsCollection.doc(id).set(prestation).catch((e) => {
            console.log(e);
        });
    }

    delete(item: PrestationI): Promise<any> {
        return this.prestationsCollection.doc(item.id).delete().catch((e) => {
            console.log(e);
        });
    }

    getPrestation(id: string): Observable<PrestationI> {
        return this.prestationsCollection.doc<PrestationI>(id).valueChanges();
    }

}

