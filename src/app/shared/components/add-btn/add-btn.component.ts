import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-add-btn',
    templateUrl: './add-btn.component.html',
    styleUrls: ['./add-btn.component.scss']
})
export class AddBtnComponent implements OnInit {

    @Input()
    text: string;

    constructor() { }

    ngOnInit() {
    }

}
