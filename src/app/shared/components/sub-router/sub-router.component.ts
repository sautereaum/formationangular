import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-sub-router',
    templateUrl: './sub-router.component.html',
    styleUrls: ['./sub-router.component.scss']
})
export class SubRouterComponent implements OnInit {

    @Input()
    routes: {url: string, name: string}[];

    constructor() { }

    ngOnInit() {
    }

}
