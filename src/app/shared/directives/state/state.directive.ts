import { Directive, DoCheck, HostBinding, Input, OnInit } from '@angular/core';
import { States } from '../../enums/states.enum';

@Directive({
    selector: '[appState]'
})
export class StateDirective implements OnInit, DoCheck {

    @Input()
    appState: States;

    @HostBinding('class')
    classes: string;

    initialClassList: string;

    constructor() { }

    ngOnInit(): void {
        this.initialClassList = this.classes ? this.classes : '';
    }

    ngDoCheck(): void {
        this.classes = this.initialClassList + this.format();
    }

    private format(): string {
        return `state-${ this.appState.normalize('NFD').replace(/[\u0300-\u036f\s]/g, '').toLowerCase() }`;
    }
}
