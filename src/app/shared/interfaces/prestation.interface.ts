import { States } from '../enums/states.enum';

export interface PrestationI {
    id: string;
    typePresta: string;
    client: string;
    tjmHt: number;
    nbJours: number;
    tauxTva: number;
    comment: string;
    state: States;

    totalHT(): number;

    totalTTC(): number;

}
