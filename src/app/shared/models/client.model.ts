import { StateClient } from '../enums/state-client.enum';
import { ClientI } from '../interfaces/client.interface';

export class Client implements ClientI {
    id: string;
    name: string;
    email: string;
    state: StateClient = StateClient.ACTIF;

    constructor(defaultFields?: Partial<ClientI>) {
        if (defaultFields) {
            Object.assign(this, defaultFields);
        }
    }

}