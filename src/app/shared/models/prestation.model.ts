import { States } from '../enums/states.enum';
import { PrestationI } from '../interfaces/prestation.interface';

export class Prestation implements PrestationI {
    client: string = null;
    comment: string = null;
    id: string = null;
    nbJours: number = null;
    state = States.OPTION;
    tauxTva = 20;
    tjmHt: number = null;
    typePresta: string = null;

    constructor(defaultFields?: Partial<PrestationI>) {
        if (defaultFields) {
            Object.assign(this, defaultFields);
        }
    }

    totalHT(): number {
        return this.tjmHt * this.nbJours;
    }

    totalTTC(tva?: number): number {
        if (tva <= 0) return this.totalHT();
        return this.totalHT() * (1 + ((tva || this.tauxTva) / 100));
    }
}