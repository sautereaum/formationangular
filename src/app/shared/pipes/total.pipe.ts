import { Pipe, PipeTransform } from '@angular/core';
import { PrestationI } from '../interfaces/prestation.interface';

@Pipe({
    name: 'total'
})
export class TotalPipe implements PipeTransform {

    transform(value: PrestationI, type?: string): number {
        if (value) {
            return type === 'TTC' ? value.totalTTC() : value.totalHT();
        }
        return Number.NaN;
    }

}
