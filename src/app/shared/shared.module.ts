import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TemplatesModule } from '../templates/templates.module';
import { AddBtnComponent } from './components/add-btn/add-btn.component';
import { BtnComponent } from './components/btn/btn.component';
import { TableauComponent } from './containers/tableau/tableau.component';
import { StateDirective } from './directives/state/state.directive';
import { TotalPipe } from './pipes/total.pipe';
import { SubRouterComponent } from './components/sub-router/sub-router.component';

@NgModule({
    declarations: [
        TotalPipe,
        TableauComponent,
        StateDirective,
        AddBtnComponent,
        BtnComponent,
        SubRouterComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        TemplatesModule,
        FontAwesomeModule
    ],
    exports: [
        TotalPipe,
        StateDirective,
        TableauComponent,
        TemplatesModule,
        AddBtnComponent,
        BtnComponent,
        SubRouterComponent,
        FontAwesomeModule
    ]
})
export class SharedModule {}
