import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    appTitle: string;

    constructor() { }

    ngOnInit() {
        this.appTitle = 'Titre de la formation Angular';
    }

}
